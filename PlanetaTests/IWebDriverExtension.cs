﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace PlanetaTests
{
	public static class IWebDriverExtension
	{
		/// <summary>
		/// Ожидает появления элемента.
		/// </summary>
		/// <param name="driver">Драйвер.</param>
		/// <param name="by">Фильтр элемента.</param>
		/// <param name="timeout">Максимально допустимое время ожидания (в мс.).</param>
		public static void WaitForElement(this IWebDriver driver, By by, int timeout = 2000) =>
			new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeout))
				.Until(d => driver.FindElement(by));

		/// <summary>
		/// Проверяет, участвует ли элемент в анимации.
		/// </summary>
		/// <param name="driver">Драйвер.</param>
		/// <param name="selector">Селектор элемента.</param>
		/// <returns>Если участвует - true, иначе - false.</returns>
		public static bool IsAnimated(this IWebDriver driver, string selector) => bool.Parse((driver as IJavaScriptExecutor)
			.ExecuteScript($"return $('{selector}').is(':animated')")
			.ToString()
			.ToLower());

		/// <summary>
		/// Вводит в поле строчку.
		/// </summary>
		/// <param name="driver">Драйвер.</param>
		/// <param name="id">Идентификатор поля.</param>
		/// <param name="keys">Вводимая строчка.</param>
		public static void SendKeysById(this IWebDriver driver, string id, string keys) => driver.FindElement(By.Id(id)).SendKeys(keys);
	}
}
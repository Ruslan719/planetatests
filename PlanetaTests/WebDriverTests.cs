﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Opera;

namespace PlanetaTests
{
	/// <summary>
	/// Базовый класс проверки в браузерах.
	/// </summary>
	public abstract class WebDriverTests
	{
		/// <summary>
		/// Драйвер, позволяющий взаимодействовать с браузером.
		/// </summary>
		protected IWebDriver driver;

		/// <summary>
		/// Конфигурация.
		/// </summary>
		public static IConfiguration configuration = new ConfigurationBuilder()
			.SetBasePath(TestContext.CurrentContext.TestDirectory)
			.AddJsonFile("appsettings.json", false, true)
			.Build();

		/// <summary>
		/// Перечисление названий браузеров, на которых проводится проверка.
		/// </summary>
		public static IEnumerable<TestCaseData> BrowserNames => configuration.GetSection("Browsers")
			.GetChildren()
			.Select(d => new TestCaseData(d.Value));

		/// <summary>
		/// Закрывает драйвер.
		/// </summary>
		[TearDown]
		public void DriverTearDown()
		{
			driver?.Quit();
		}

		/// <summary>
		/// Создает драйвер по названию браузера.
		/// </summary>
		/// <param name="browser_name">Названию браузера.</param>
		/// <returns>Драйвер.</returns>
		protected IWebDriver GetDriver(string browser_name)
		{
			string drivers_path = $@"{TestContext.CurrentContext.TestDirectory}\Drivers\";
			switch (browser_name)
			{
				case "chrome":
					return new ChromeDriver(drivers_path);
				case "opera":
					// Необходимо явно указывать исполняемый файл браузера Opera.
					var options = new OperaOptions {BinaryLocation = configuration["OperaLauncher"]};
					return new OperaDriver(drivers_path, options);
				case "firefox":
					return new FirefoxDriver(drivers_path);
				default:
					return new ChromeDriver(drivers_path);
			}
		}
	}
}
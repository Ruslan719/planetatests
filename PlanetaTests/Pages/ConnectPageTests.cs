﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace PlanetaTests
{
	/// <summary>
	/// Проверяет страницу подключения.
	/// </summary>
	[TestFixture]
	public class ConnectPageTests : WebDriverTests
	{
		[Test]
		[TestCaseSource(nameof(BrowserNames))]
		public void Task3(string browser_name)
		{
			driver = GetDriver(browser_name);
			driver.Navigate()
				.GoToUrl(MainPageTests.main_page_url);

			// Переходим на страницу подключения.
			driver.FindElement(By.Id("widget_form"))
				.FindElements(By.TagName("div"))
				.First(e => e.Text == "Включить\r\nПланету")
				.Click();

			CheckAddress_ConnectionPossible();
			SendDemand_DemandAccepted();
		}
		
		/// <summary>
		/// Проверяет доступность подключения для некоторого адреса.
		/// Перед выполнением нужно перейти на страницу подключения.
		/// </summary>
		public void CheckAddress_ConnectionPossible()
		{
			// Определяем элементы.
			IWebElement step1_form = driver.FindElement(By.Id("step1_form"));
			IWebElement city_field = step1_form.FindElements(By.ClassName("wizard-field"))
				.First(e => e.FindElements(By.TagName("label")).Any(el => el.Text == "Населенный пункт"));
			ReadOnlyCollection<IWebElement> city_list = city_field.FindElement(By.ClassName("wizard-select__list"))
				.FindElements(By.XPath(".//*"));
			IWebElement room = driver.FindElement(By.Id("address_flat"));
			IWebElement step1_submit_button = driver.FindElement(By.Id("step1_submit"));

			// Заполняем поля.
			driver.FindElement(By.Id("wizard-city-select")).Click();
			IWebElement city = city_list.First(c => c.Text == configuration["ConnectionAddress:City"]);
			city.Click();
			(driver as IJavaScriptExecutor).ExecuteScript($"document.getElementById('address_city').setAttribute('value', '{city.GetAttribute("data-index")}')");
			driver.SendKeysById("address_street", configuration["ConnectionAddress:Street"]);
			driver.SendKeysById("address_house", configuration["ConnectionAddress:BuildingName"]);
			step1_submit_button.Click();

			try
			{
				// Пытаемся дождаться поля для ввода квартиры.
				var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(1));
				wait.Until(d => room.Displayed);
				room.SendKeys(configuration["ConnectionAddress:Room"]);
				step1_submit_button.Click();
			}
			catch (WebDriverTimeoutException) { }
			// Перешли на другую страницу.
			catch (StaleElementReferenceException) { }

			Assert.IsTrue(driver.FindElements(By.TagName("p")).Any(e => e.Text == "Мы рады возможности подключить вас по указанному адресу."),
				"Не удалось подтвердить возможность подключения.");
		}

		/// <summary>
		/// Проверяет создание заявки при указанном адресе.
		/// </summary>
		public void SendDemand_DemandAccepted()
		{
			driver.SendKeysById("connect_first_name", "DIRI521-5500 autotest");
			driver.SendKeysById("connect_phone", "0000000000");
			driver.SendKeysById("connect_email", "111@11.ru");

			driver.FindElement(By.Id("step2_submit")).Click();

			Assert.IsTrue(driver.FindElements(By.TagName("h3")).Any(e => e.Text == "Заявка принята"),
				"Не удалось создать заявку.");
		}
	}
}
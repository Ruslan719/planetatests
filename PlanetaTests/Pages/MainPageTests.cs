﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace PlanetaTests
{
	/// <summary>
	/// Проверяет главную страницу.
	/// </summary>
	[TestFixture]
	public class MainPageTests : WebDriverTests
	{
		#region Fields

		/// <summary>
		/// Максимальное допустимое время на переключение вкладки меню.
		/// </summary>
		public const int PANES_SWITCH_TIMEOUT = 1500;

		/// <summary>
		/// Текст на кнопке перехода к сравнению продуктов.
		/// </summary>
		public const string COMPARE_TARIFF_BUTTON_TEXT = "Сравнить продукты";

		/// <summary>
		/// Текст на кнопке перехода к пополнению баланса.
		/// </summary>
		public const string RECHARGE_BUTTON_TEXT = "Пополнить\r\nбаланс";

		/// <summary>
		/// Класс элемента меню.
		/// </summary>
		public const string MENU_ELEMENTS_CLASS = "NavigationMenuItem";

		/// <summary>
		/// Класс ссылки на элементе меню.
		/// </summary>
		public const string MENU_LINKS_CLASS = "NavigationMenuItem__inner";

		/// <summary>
		/// Класс вкладки меню.
		/// </summary>
		public const string PANES_CLASS = "PanesItem";

		/// <summary>
		/// Класс активной вкладки меню.
		/// </summary>
		public const string ACTIVE_PANES_CLASS = "PanesItem--active";

		/// <summary>
		/// URL главной страницы.
		/// </summary>
		public static string main_page_url = configuration["Url"];

		#endregion

		[Test]
		[TestCaseSource(nameof(BrowserNames))]
		public void Task1(string browser_name)
		{
			driver = GetDriver(browser_name);
			driver.Navigate()
				.GoToUrl(main_page_url);

			HoverMenuButtons_ColorChanged();
			HoverOpenedMenuButtons_PanesSwiched();
			ClickClosePanesButton_PanesClosed();
		}

		/// <summary>
		/// Проверяет, что при наведении на элементы меню меняется цвет ссылки.
		/// </summary>
		public void HoverMenuButtons_ColorChanged()
		{
			IWebElement menu = driver.FindElement(By.ClassName("NavigationMenu"));

			foreach (IWebElement menu_element in menu.FindElements(By.ClassName(MENU_LINKS_CLASS)))
			{
				var actions = new Actions(driver);
				actions.MoveToElement(menu_element)
					.Perform();
				
				Assert.IsTrue(menu_element.GetCssValue("color").Contains("236, 0, 140"),
					$"Пункт меню \"{menu_element.Text}\" не сменил цвет при наведении.");
			}
		}

		/// <summary>
		/// Проверяет прокрутку плашки открытого меню при наведении.
		/// </summary>
		public void HoverOpenedMenuButtons_PanesSwiched()
		{
			ReadOnlyCollection<IWebElement> menu_elements = driver.FindElements(By.ClassName(MENU_ELEMENTS_CLASS));
			ReadOnlyCollection<IWebElement> panes_elements = driver.FindElements(By.ClassName(PANES_CLASS));
			
			menu_elements.First()
				.Click();

			var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(PANES_SWITCH_TIMEOUT));
			for (int i = 0; i < menu_elements.Count; ++i)
			{
				var actions = new Actions(driver);
				actions.MoveToElement(menu_elements[i])
					.Perform();
				
				string menu_element_name = menu_elements[i].FindElement(By.ClassName(MENU_LINKS_CLASS)).Text;
				Assert.DoesNotThrow(() => wait.Until(d => panes_elements[i].GetAttribute("class").Contains(ACTIVE_PANES_CLASS)),
					$"Плашка не прокрутилась при переключении на \"{menu_element_name}\".");

				Assert.IsNotEmpty(panes_elements[i].FindElements(By.LinkText(COMPARE_TARIFF_BUTTON_TEXT)),
					$"Не найден пункт \"{COMPARE_TARIFF_BUTTON_TEXT}\" во вкладке \"{menu_element_name}\"");
				Assert.IsNotEmpty(panes_elements[i].FindElements(By.ClassName("Media__body")).Where(e => e.Text == RECHARGE_BUTTON_TEXT),
					$"Не найден пункт \"{RECHARGE_BUTTON_TEXT}\" во вкладке \"{menu_element_name}\"");
			}
		}

		/// <summary>
		/// Проверяет работоспособность кнопки закрытия вкладок.
		/// </summary>
		public void ClickClosePanesButton_PanesClosed()
		{
			ReadOnlyCollection<IWebElement> menu_elements = driver.FindElements(By.ClassName(MENU_ELEMENTS_CLASS));
			ReadOnlyCollection<IWebElement> panes_elements = driver.FindElements(By.ClassName(PANES_CLASS));
			IWebElement close_button = driver.FindElement(By.ClassName("Navigation__close"));

			for (int i = 0; i < menu_elements.Count; ++i)
			{
				var actions = new Actions(driver);
				actions.Click(menu_elements[i])
					.Click(close_button)
					.Perform();

				string menu_element_name = menu_elements[i].FindElement(By.ClassName(MENU_LINKS_CLASS)).Text;
				Assert.IsFalse(panes_elements[i].GetAttribute("class").Contains(ACTIVE_PANES_CLASS),
					$"Не закрылась вкладка \"{menu_element_name}\".");
			}
		}
	}
}
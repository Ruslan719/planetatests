﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace PlanetaTests
{
	/// <summary>
	/// Проверяет страницу продуктов.
	/// </summary>
	[TestFixture]
	public class TariffPageTests : WebDriverTests
	{
		/// <summary>
		/// Блок текста, который должен отображаться на страницах просмотра продуктов.
		/// </summary>
		public const string ADVERTISING_TEXT = "Обратите внимание на раздел Акции. В нем содержится актуальная информация о специальных предложениях, скидках на оборудование и возможностях получения бонусных баллов — чатлов, с помощью которых вы можете компенсировать затраты по тарифам «Планеты» на интернет и другие услуги.";

		/// <summary>
		/// Класс ссылки на страницу продукта.
		/// </summary>
		public const string TARIFF_NAMES_CLASS = "choice-name__link";

		/// <summary>
		/// Класс кнопки выбора продукта.
		/// </summary>
		public const string SELECT_BUTTON_CLASS = "choice__button";

		[Test]
		[TestCaseSource(nameof(BrowserNames))]
		public void Task2(string browser_name)
		{
			driver = GetDriver(browser_name);
			driver.Navigate()
				.GoToUrl(MainPageTests.main_page_url);

			// Переходим на страницу продуктов.
			driver.FindElements(By.ClassName(MainPageTests.MENU_ELEMENTS_CLASS))
				.First(e => e.Text == "ПРОДУКТЫ")
				.Click();
			driver.FindElement(By.ClassName(MainPageTests.ACTIVE_PANES_CLASS))
				.FindElement(By.ClassName("NavigationList__item"))
				.Click();

			ConsiderTariffs_AdvertisingTextExists();

			// Переходим на страницу сравнения продуктов.
			driver.FindElement(By.ClassName(MainPageTests.MENU_ELEMENTS_CLASS))
				.Click();
			driver.FindElement(By.LinkText(MainPageTests.COMPARE_TARIFF_BUTTON_TEXT))
				.Click();
			// Строки формируются дополнительным запросом, нужно дождаться их загрузки.
			driver.WaitForElement(By.ClassName(TARIFF_NAMES_CLASS));

			NavigateToTariffs_SuccessfulTransitions();
			HoverTariffRows_SelectButtonsExists();
		}

		/// <summary>
		/// Проверяет, что на страницах просмотра продуктов есть блок текста с ссылками.
		/// Перед выполнением нужно перейти на страницу просмотра продуктов.
		/// </summary>
		public void ConsiderTariffs_AdvertisingTextExists()
		{
			ReadOnlyCollection<IWebElement> tab_items = driver.FindElements(By.ClassName("tariff-tab__item"));
			string articles_url = $"{MainPageTests.main_page_url}articles/inet/";

			foreach (IWebElement tab_item in tab_items)
			{
				tab_item.Click();
				
				// Ждем переключения на вкладку.
				var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(1));
				wait.Until(d => !d.IsAnimated(".tariffLineCursor"));

				IWebElement text = driver.FindElements(By.TagName("p"))
					.FirstOrDefault(e => e.Text == ADVERTISING_TEXT);
				Assert.IsNotNull(text, $"Блок с текстом \"{ADVERTISING_TEXT}\" отсутствует");

				ReadOnlyCollection<IWebElement> hrefs = text.FindElements(By.TagName("a"));
				string href_attribute_name = "href";
				Assert.IsTrue(hrefs.Any(r => r.Text == "Акции" && r.GetAttribute(href_attribute_name).StartsWith($"{articles_url}actions")),
					"В тексте отсутствует ссылка на страницу акций.");
				Assert.IsTrue(hrefs.Any(r => r.Text == "оборудование" && r.GetAttribute(href_attribute_name).StartsWith($"{articles_url}products#digital-home")),
					"В тексте отсутствует ссылка на страницу с оборудованием.");
				Assert.IsTrue(hrefs.Any(r => r.Text == "интернет" && r.GetAttribute(href_attribute_name).StartsWith($"{articles_url}description")),
					"В тексте отсутствует ссылка на страницу подключения интернета.");
			}
		}

		/// <summary>
		/// Проверяет переходы на страницы продуктов по ссылкам со страницы сравнения.
		/// Перед выполнением нужно перейти на страницу сравнения продуктов.
		/// </summary>
		public void NavigateToTariffs_SuccessfulTransitions()
		{
			ReadOnlyCollection<IWebElement> hrefs = driver.FindElements(By.ClassName(TARIFF_NAMES_CLASS));

			for (int i = 0; i < hrefs.Count; ++i)
			{
				string tariff_name = hrefs[i].Text;
				hrefs[i].Click();

				Assert.AreEqual(tariff_name, driver.FindElement(By.Id("rate"))
					.FindElement(By.ClassName("active"))
					.FindElement(By.ClassName("rate__title")).Text,
					$"Переход на страницу продукта {tariff_name} не осуществился.");

				driver.Navigate()
					.Back();
				// Прошлые сущности после переходов недействительны.
				hrefs = driver.FindElements(By.ClassName(TARIFF_NAMES_CLASS));
			}
		}

		/// <summary>
		/// Проверяет, что кнопка выбора появляется при наведении на строку продукта и пропадает при потере фокуса.
		/// Перед выполнением нужно перейти на страницу сравнения продуктов.
		/// </summary>
		public void HoverTariffRows_SelectButtonsExists()
		{
			ReadOnlyCollection<IWebElement> tariff_rows = driver.FindElements(By.ClassName("choice__row"));
			IWebElement menu_element = driver.FindElement(By.ClassName(MainPageTests.MENU_ELEMENTS_CLASS));

			int i = 0;
			foreach (var row in tariff_rows)
			{
				// Костыль. В Firefox скролл не работает при выполнении MoveToElement. https://github.com/SeleniumHQ/selenium/issues/4148
				(driver as IJavaScriptExecutor).ExecuteScript($"document.getElementsByClassName('choice__row')[{i}].scrollIntoView()");
				++i;
				var actions = new Actions(driver);
				actions.MoveToElement(row)
					.Perform();

				IWebElement select_button = row.FindElement(By.ClassName(SELECT_BUTTON_CLASS));

				string tariff_name = row.FindElement(By.ClassName(TARIFF_NAMES_CLASS)).Text;
				Assert.IsTrue(select_button.Displayed, $"Кнопка \"Выбрать\" не отображается при наведении на строку продукта {tariff_name}.");
				
				actions = new Actions(driver);
				actions.MoveToElement(menu_element)
					.Perform();

				Assert.IsTrue(driver.FindElements(By.ClassName(SELECT_BUTTON_CLASS)).All(e => !e.Displayed),
					$"Кнопка \"Выбрать\" отображается при потере фокуса на строке продукта {tariff_name}.");
			}
		}
	}
}